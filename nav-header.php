<?php  ?>


 <!DOCTYPE html>
<html lang="en">

<!-- begin::Head -->
<head>
    <base href="">
    <meta charset="utf-8" />
    <title>Tablero Control | Estudio de Abogados</title>
    <meta name="description" content="Latest updates and statistic charts">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <link rel="stylesheet" href="../lib/alertify/css/alertify.min.css" />
    <link rel="stylesheet" href="../lib/alertify/css/themes/default.min.css" />
    <link rel="stylesheet" href="../lib/lineAwesome/css/line-awesome.min.css" />
    <!--end::Fonts -->
    <!--begin::Page Vendors Styles(used by this page) -->
    <link href="../assets/plugins/custom/fullcalendar/fullcalendar.bundle.css" rel="stylesheet" type="text/css" />

    <!--end::Page Vendors Styles -->
    <!--begin::Global Theme Styles(used by all pages) -->
    <link href="../assets/plugins/global/plugins.bundle.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/style.bundle.css" rel="stylesheet" type="text/css" />

    <!--end::Global Theme Styles -->
    <!--begin::Layout Skins(used by all pages) -->
    <link href="../assets/css/skins/header/base/light.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/skins/header/menu/light.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/skins/brand/navy.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/skins/aside/navy.css" rel="stylesheet" type="text/css" />
    <link href="../css/site.css" rel="stylesheet" type="text/css" />

    <!--end::Layout Skins -->
    <link rel="shortcut icon" href="../assets/media/logos/favicon.ico" />
</head>

<!-- end::Head -->
<!-- begin::Body -->
<body  class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--enabled kt-subheader--transparent kt-aside--enabled kt-aside--fixed kt-page--loading">

	<?php include 'ventanaModal.php' ?>
    
    
    <!-- begin:: Root -->
    <div class="kt-grid kt-grid--hor kt-grid--root">

        <!-- begin:: Page -->
        <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">

            <!-- begin:: Aside -->
            <button class="kt-aside-close " id="kt_aside_close_btn"><i class="la la-close"></i></button>
            <div class="kt-aside  kt-aside--fixed bg-color-sidebar" id="kt_aside">

                <div class="kt-aside__brand kt-grid__item bg-color-sidebar" id="kt_aside_brand">
                    <div class="kt-aside__brand-logo">
                        <a href="#">
                            <!--<img alt="Logo" src="img/logo-setra.jpg" style="width:88px; height:50px;"/>-->
                        </a>
                    </div>
                    <div class="kt-aside__brand-tools">
                        <button class="kt-aside__brand-aside-toggler kt-aside__brand-aside-toggler--left" id="kt_aside_toggler"><span></span></button>
                    </div>
                </div>

                <div class="kt-aside-menu-wrapper kt-grid__item kt-grid__item--fluid" id="kt_aside_menu_wrapper">
                    <div id="kt_aside_menu" class="kt-aside-menu bg-color-sidebar" data-ktmenu-vertical="1" data-ktmenu-scroll="1" data-ktmenu-dropdown-timeout="500">
                        <ul class="kt-menu__nav ">
                            <li class="kt-menu__item  kt-menu__item--submenu kt-menu__item--open kt-menu__item--here" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
                                <a href="Dashboard.php" class="kt-menu__link bg-color-sidebar"><i class="kt-menu__link-icon flaticon2-graphic"></i><span class="kt-menu__link-text">Dashboards</span></a>
                            </li>
                            <li class="kt-menu__section ">
                                <h4 class="kt-menu__section-text colorTituloGrupal">Recepcion</h4>
                                <i class="kt-menu__section-icon flaticon-more-v2"></i>
                            </li>

                            <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
                                <a href="NewProcess.php" class="kt-menu__link"><i class="kt-menu__link-icon flaticon2-copy"></i><span class="kt-menu__link-text">Proceso Judicial</span></a>
                                <div class="kt-menu__submenu ">
                                    <span class="kt-menu__arrow"></span>
                                </div>
                            </li>
                            <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
                                <a href="Disponibilidad.php" class="kt-menu__link"><i class="kt-menu__link-icon flaticon2-checking"></i><span class="kt-menu__link-text">Disponibilidad</span></a>
                                <div class="kt-menu__submenu ">
                                    <span class="kt-menu__arrow"></span>
                                </div>
                            </li>
                            <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
                                <a class="kt-menu__link"><i class="kt-menu__link-icon flaticon2-copy"></i><span class="kt-menu__link-text">Tarifario</span></a>
                                <div class="kt-menu__submenu ">
                                    <span class="kt-menu__arrow"></span>
                                </div>
                            </li>
                            <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
                                <a class="kt-menu__link"><i class="kt-menu__link-icon flaticon2-percentage "></i><span class="kt-menu__link-text">Honorario de Abogados</span></a>
                                <div class="kt-menu__submenu ">
                                    <span class="kt-menu__arrow"></span>
                                </div>
                            </li>

                            <li class="kt-menu__section ">
                                <h4 class="kt-menu__section-text colorTituloGrupal">Abogados</h4>
                                <i class="kt-menu__section-icon flaticon-more-v2"></i>
                            </li>

                            <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
                                <a href="Cronograma.php" class="kt-menu__link "><i class="kt-menu__link-icon flaticon2-calendar-2"></i><span class="kt-menu__link-text">Cronograma</span></a>
                                <div class="kt-menu__submenu ">
                                    <span class="kt-menu__arrow"></span>
                                </div>
                            </li>
                            <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
                                <a href="Ordenes.php" class="kt-menu__link"><i class="kt-menu__link-icon flaticon2-copy"></i><span class="kt-menu__link-text">Ordenes Asignadas</span></a>
                                <div class="kt-menu__submenu ">
                                    <span class="kt-menu__arrow"></span>
                                </div>
                            </li>
                            <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
                                <a class="kt-menu__link "><i class="kt-menu__link-icon flaticon2-gear"></i><span class="kt-menu__link-text">Mantenimiento de Caso</span></a>
                                <div class="kt-menu__submenu ">
                                    <span class="kt-menu__arrow"></span>
                                </div>
                            </li>
                            <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
                                <a class="kt-menu__link"><i class="kt-menu__link-icon flaticon2-contract"></i><span class="kt-menu__link-text">Casos Asignados</span></a>
                                <div class="kt-menu__submenu ">
                                    <span class="kt-menu__arrow"></span>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>

                <!-- begin:: Aside Footer -->
                <div class="kt-aside__footer kt-grid__item" id="kt_aside_footer">
                    <div class="kt-aside__footer-nav bg-color-sidebar">
                        <div class="kt-aside__footer-item">
                            <a href="#" class="btn btn-icon"><i class="flaticon2-gear"></i></a>
                        </div>
                        <div class="kt-aside__footer-item">
                            <a href="#" class="btn btn-icon"><i class="flaticon2-cube"></i></a>
                        </div>
                        <div class="kt-aside__footer-item">
                            <a href="#" class="btn btn-icon"><i class="flaticon2-bell-alarm-symbol"></i></a>
                        </div>
                        <div class="kt-aside__footer-item">
                            <button type="button" class="btn btn-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="flaticon2-add"></i>
                            </button>
                            <div class="dropdown-menu dropdown-menu-left">
                                <ul class="kt-nav">
                                    <li class="kt-nav__section kt-nav__section--first">
                                        <span class="kt-nav__section-text">Export Tools</span>
                                    </li>
                                    <li class="kt-nav__item">
                                        <a href="#" class="kt-nav__link">
                                            <i class="kt-nav__link-icon la la-print"></i>
                                            <span class="kt-nav__link-text">Print</span>
                                        </a>
                                    </li>
                                    <li class="kt-nav__item">
                                        <a href="#" class="kt-nav__link">
                                            <i class="kt-nav__link-icon la la-copy"></i>
                                            <span class="kt-nav__link-text">Copy</span>
                                        </a>
                                    </li>
                                    <li class="kt-nav__item">
                                        <a href="#" class="kt-nav__link">
                                            <i class="kt-nav__link-icon la la-file-excel-o"></i>
                                            <span class="kt-nav__link-text">Excel</span>
                                        </a>
                                    </li>
                                    <li class="kt-nav__item">
                                        <a href="#" class="kt-nav__link">
                                            <i class="kt-nav__link-icon la la-file-text-o"></i>
                                            <span class="kt-nav__link-text">CSV</span>
                                        </a>
                                    </li>
                                    <li class="kt-nav__item">
                                        <a href="#" class="kt-nav__link">
                                            <i class="kt-nav__link-icon la la-file-pdf-o"></i>
                                            <span class="kt-nav__link-text">PDF</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="kt-aside__footer-item">
                            <a href="#" class="btn btn-icon"><i class="flaticon2-calendar-2"></i></a>
                        </div>
                    </div>
                </div>
                <!-- end:: Aside Footer-->
            </div>

            <!-- end:: Aside -->
            <!-- begin:: Wrapper -->
            <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">

                <!-- begin:: Header -->
                <div id="kt_header" class="kt-header kt-grid__item  kt-header--fixed ">

                    <!-- begin:: Header Menu -->
                    <button class="kt-header-menu-wrapper-close" id="kt_header_menu_mobile_close_btn"><i class="la la-close"></i></button>
                    <div class="kt-header-menu-wrapper" id="kt_header_menu_wrapper">
                        
                    </div>

                    <!-- end:: Header Menu -->
                    <!-- begin:: Header Topbar -->
                    <div class="kt-header__topbar">
                        <!--begin:: Languages -->
                        <div class="kt-header__topbar-item kt-header__topbar-item--langs">
                            <div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="10px,0px">
                                <span class="kt-header__topbar-icon kt-rounded-">
                                    <img class="" src="../assets/media/flags/020-flag.svg" alt="" />
                                </span>
                            </div>
                            <div class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-top-unround">
                                <ul class="kt-nav kt-margin-t-10 kt-margin-b-10">
                                    <li class="kt-nav__item">
                                        <a href="#" class="kt-nav__link">
                                            <span class="kt-nav__link-icon"><img src="../assets/media/flags/016-spain.svg" alt="" /></span>
                                            <span class="kt-nav__link-text">Spanish</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <!--end:: Languages -->
                        <!--begin: User Bar -->
                        <div class="kt-header__topbar-item kt-header__topbar-item--user">
                            <div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="0px,0px">

                                <!--use "kt-rounded" class for rounded avatar style-->
                                <div class="kt-header__topbar-user kt-rounded-">
                                    <span class="kt-header__topbar-welcome kt-hidden-mobile">Hi,</span>
                                    <span class="kt-header__topbar-username kt-hidden-mobile">Santa</span>
                                    <img alt="Pic" src="../assets/media/users/default.jpg" class="kt-rounded-" />

                                    <!--use below badge element instead the user avatar to display username's first letter(remove kt-hidden class to display it) -->
                                    <span class="kt-badge kt-badge--username kt-badge--lg kt-badge--brand kt-hidden kt-badge--bold">S</span>
                                </div>
                            </div>
                            <div class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-top-unround dropdown-menu-sm">
                                <div class="kt-user-card kt-margin-b-40 kt-margin-b-30-tablet-and-mobile">
                                    <div class="kt-user-card__wrapper">
                                        <div class="kt-user-card__pic">

                                            <!--use "kt-rounded" class for rounded avatar style-->
                                            <img alt="Pic" src="../assets/media/users/default.jpg" class="kt-rounded-" />
                                        </div>
                                        <div class="kt-user-card__details">
                                            <div class="kt-user-card__name">Luis</div>
                                            <div class="kt-user-card__position">Santa.</div>
                                        </div>
                                    </div>
                                </div>
                                <ul class="kt-nav kt-margin-b-10">
                                    
                                    <li class="kt-nav__separator kt-nav__separator--fit"></li>
                                    <li class="kt-nav__custom kt-space-between">
                                        <a href="#" class="btn btn-profile btn-upper btn-sm" data-toggle="modal" data-target="#logoutModal">Sign Out</a>
                                        <i class="flaticon2-information kt-label-font-color-2" data-toggle="kt-tooltip" data-placement="right" title="" data-original-title="Click to learn more..."></i>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <!--end: User Bar -->
                    </div>

                    <!-- end:: Header Topbar -->
                </div>
                <!-- end:: Header -->

                <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

                    <!-- begin:: Subheader -->
                    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
                        <div class="kt-container  kt-container--fluid ">
                            <div class="kt-subheader__main">
                                <h3 class="kt-subheader__title">Dashboard</h3>
                                <span class="kt-subheader__separator kt-hidden"></span>
                                <div class="kt-subheader__breadcrumbs">
                                    <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter bg-color-navbar"></i></a>
                                    <span class="kt-subheader__breadcrumbs-separator"></span>
                                    <a href="#" class="kt-subheader__breadcrumbs-link bg-color-navbar">
                                        Dashboards
                                    </a>
                                    <span class="kt-subheader__breadcrumbs-separator"></span>
                                    <a href="" class="kt-subheader__breadcrumbs-link bg-color-navbar">
                                        Navy Aside
                                    </a>
								</div>
                            </div>
                           
                        </div>
                    </div>

                    <!-- end:: Subheader -->
                    <!-- begin:: Content -->
                   	<div class="container-fluid">
                        <div id="containerPrimary">
                            <div class="row">
				              <div class="col-md-4">
				                  
				              </div>

				              <div class="col-md-4">
				                <div class="card">
				                    <div class="card-header" style="text-align: center;">
				                       Proceso Judicial
				                    </div>
				                    <div class="card-body" style="text-align: center;">
				                      <button type="button" class="btn btn-info" onclick="Nuevo_proceso();">Nuevo (+)</button>
				                    </div>
				                </div>
				                
				              </div>

				              <div class="col-md-4">
				                
				              </div>
				            
				          </div>
                        </div>

                        <div id="containerSecondary" class="d-none"></div>

                    </div>
                    
                    <!-- end:: Content -->
                </div>
                  
            </div>

            <!-- end:: Wrapper -->
        </div>
        
        <!-- end:: Page -->
    </div>

    <!-- end:: Root -->
    

    <!-- end::Offcanvas Toolbar Quick Actions -->
    
    <!-- begin:: Scrolltop -->
    <div id="kt_scrolltop" class="kt-scrolltop">
        <i class="la la-arrow-up"></i>
    </div>

    <script>
        var KTAppOptions = {
            "colors": {
                "state": {
                    "brand": "#5d78ff",
                    "metal": "#c4c5d6",
                    "light": "#ffffff",
                    "accent": "#00c5dc",
                    "primary": "#5867dd",
                    "success": "#34bfa3",
                    "info": "#36a3f7",
                    "warning": "#ffb822",
                    "danger": "#fd3995",
                    "focus": "#9816f4"
                },
                "base": {
                    "label": [
                        "#c5cbe3",
                        "#a1a8c3",
                        "#3d4465",
                        "#3e4466"
                    ],
                    "shape": [
                        "#f0f3ff",
                        "#d9dffa",
                        "#afb4d4",
                        "#646c9a"
                    ]
                }
            }
        };
    </script>

    <!-- end::Global Config -->
    <!--begin::Global Theme Bundle(used by all pages) -->
    <script src="../assets/plugins/global/plugins.bundle.js" type="text/javascript"></script>
    <script src="../assets/js/scripts.bundle.js" type="text/javascript"></script>
    <!--end::Global Theme Bundle -->
    <!--begin::Page Vendors(used by this page) -->
    <script src="../assets/plugins/custom/fullcalendar/fullcalendar.bundle.js" type="text/javascript"></script>
    <!--end::Page Vendors -->
    <!--begin::Page Scripts(used by this page) -->
    <script src="../assets/js/pages/dashboard.js" type="text/javascript"></script>
     <script src="../assets/js/pages/components/extended/sweetalert2.js"></script>
    <script src="../lib/alertify/alertify.min.js"></script>
    <script src="../config/env.js"></script>
    <script src="../config/constant.js"></script>
    <script src="../js/helper/api.js"></script>
    <script src="../js/helper/popup.js"></script>
    <script src="../js/site.js" asp-append-version="true"></script>

</body>
<!-- end::Body -->
</html>


