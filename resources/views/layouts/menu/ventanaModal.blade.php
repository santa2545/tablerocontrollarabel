<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Esta seguro que desea salir?</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">Seleccione "Salir" para cerrar su sesion.</div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                        <a class="dropdown-item" href="{{ route('logout') }}"
               onclick="event.preventDefault();
                             document.getElementById('logout-form').submit();">
                {{ __('SALIR') }}
            </a>

            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
           <!-- <a class="btn btn-primary" href="#">Salir</a> -->
          </div>
        </div>
      </div>
</div>

<!--Modal Nuevo Proceso-->
  <div class="modal" id="modal-new-process">
    <div class="modal-dialog">
      <div class="modal-content">

        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Nuevo Proceso</h4><br>


        </div>
        <form id="formLimpiar" >
        <!-- Modal body -->
        <div class="modal-body">

                <div class="row">
                  <div class="col-md-2"></div>
                  <label class="label-texto col-md-2">Nº Proc:</label>
                  <input type="text"  id="num_proceso" name="num_proceso" class="col-md-6  form-control" disabled>
                </div>
                <div class="row">
                  <div class="col-md-2"></div>
                  <label class="label-texto col-md-2">Proceso:</label>
                  <select class="form-control col-md-6 process" id="proceso" name="proceso">

                  </select>
                </div>
                  <div class="col-md-2"></div>
                <div class="row">
                  <div class="col-md-2"></div>
                  <label class="label-texto col-md-2">Tipo Proc:</label>
                  <div class=" col-md-6 without-padding" id="selectTipoProceso" >
                  </div>

                </div>
                <div class="row">
                  <div class="col-md-2"></div>
                  <label class="label-texto col-md-2">Tipo Sol:</label>
                  <select class="form-control col-md-6" id="tipo_solicitante" name="tipo_solicitante">
                    <option value="1">Demandante</option>
                    <option value="2">Demandandado</option>
                  </select>
                </div>

                <div class="row">
                  <div class="col-md-2"></div>
              	  <label class="label-texto col-md-2">Nombres:</label>
                  <input type="text"  id="nombre" name="nombre" class="col-md-6  form-control" placeholder="Ingrese Nombres" required>
                </div>

                <div class="row">
                  <div class="col-md-2"></div>
                  <label class="label-texto col-md-2">Apellidos:</label>
                  <input type="text"  id="apellido" name="apellido" class="col-md-6 form-control" placeholder="Ingrese Apellidos" required>
                </div>

                <div class="row">
                  <div class="col-md-2"></div>
                  <label class="label-texto col-md-2">Edad:</label>
                  <input type="text"  id="edad" name="edad" class="col-md-6 form-control" placeholder="Ingrese Edad" required>
                </div>
                <div class="row">
                  <div class="col-md-2"></div>
                  <label class="label-texto col-md-2">Telefono:</label>
                 <input type="text" id="telefono" name="telefono" class="col-md-6 form-control" placeholder="Ingrese Nº Telefono" maxlength="9" required>
                </div>
                <div class="row">
                  <div class="col-md-2"></div>
                  <label class="label-texto col-md-2">Direccion:</label>
                  <input type="text"  id="direccion" name="direccion" class="col-md-6 form-control" placeholder="Ingrese Direccion" required>
                </div>
                <div class="row">
                  <div class="col-md-2"></div>
                  <label class="label-texto col-md-2">DNI:</label>
                  <input type="text"  id="dni" name="dni" class="col-md-6 form-control" placeholder="Ingrese Documento de Identidad" required>
                </div>
                <div class="row">
                  <div class="col-md-2"></div>
                  <label class="label-texto col-md-2">Distrito:</label>
                  <select class="form-control col-md-6" id="distrito" name="distrito">

                  </select>
                </div>
                <div class="row">
                  <div class="col-md-2"></div>
                  <label class="label-texto col-md-2">Juzgado:</label>
                  <select class="form-control col-md-6" id="juzgado" name="juzgado">

                  </select>
                </div>
                <div class="row">
                  <div class="col-md-2"></div>
                  <label class="label-texto col-md-2">Abogado:</label>
                  <select class="form-control col-md-6" id="abogado" name="abogado">
                    <option value="1">Abogado a</option>
                    <option value="2">Abogado b</option>
                  </select>
                </div>
                <div class="row">
                  <div class="col-md-2"></div>
                  <label class="label-texto col-md-2">Fecha:</label>
                  <input type="datetime-local"  id="fecha" name="fecha" class="col-md-6 form-control" placeholder="Ingrese Distrito" required>
                </div>

        </div>

        <!-- Modal footer -->
        <div class="modal-footer">
         <button type="button" id="btnLimpiar" class="btn btn-info"  onclick='registrar_proceso();'>REGISTRAR</button>
          <button type="button" class="btn btn-danger" data-dismiss="modal">CERRAR</button>
        </div>
        </form>


          </div>
      </div>
  </div>
<!--Fin Modal-->

<!--Modal Nueva Persona-->
  <div class="modal" id="modal-new-person">
    <div class="modal-dialog">
      <div class="modal-content">

        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Nueva Persona</h4><br>


        </div>
        <form id="formLimpiarPerson" method="POST" action="{{ route('personas.store')}}">
            @csrf
        <!-- Modal body -->
        <div class="modal-body">

                <div class="row">
                  <div class="col-md-2"></div>
                  <label class="label-texto col-md-2">Rol:</label>
                  <select class="form-control col-md-6 process" id="idrol" name="idrol">
                    @foreach (App\RolPersona::all() as $item)
                    <option value="{{$item->idRol}}">{{$item->rol}}</option>
                    @endforeach
                    
                  </select>
                      <div class="col-md-2">
           
                      </div>
                </div>
                
                <div class="row">
                  <div class="col-md-2"></div>
                  <label class="label-texto col-md-2">Nombre:</label>
                  <input type="text"  id="nombre" name="nombre" class="col-md-6  form-control" placeholder="Ingrese Nombres">
                  <div class="col-md-2"></div>
                </div>
                
                <div class="row">
                  <div class="col-md-2"></div>
                  <label class="label-texto col-md-2">Documento:</label>
                  <input type="text"  id="documento" name="documento" class="col-md-6  form-control" placeholder="Ingrese Documento">
                  <div class="col-md-2"></div>
                </div>
                <div class="row">
                  <div class="col-md-2"></div>
                  <label class="label-texto col-md-2">Genero:</label>
                  <select class="form-control col-md-6" id="genero" name="genero">
                     
                      <option value="{{App\Persona::GENERO_M}}">MASCULINO</option>
                      <option value="{{App\Persona::GENERO_F}}">FEMENINO</option>
                      <option value="{{App\Persona::GENERO_OTROS}}">OTROS</option>
                  </select>
                </div>

                <div class="row">
                  <div class="col-md-2"></div>
                  <label class="label-texto col-md-2">Tipo Documento:</label>
                  <select class="form-control col-md-6" id="tipo_docu" name="tipo_docu">
                      <option value="{{App\Persona::TIPO_DOCU_DNI}}">DNI</option>
                      <option value="{{App\Persona::TIPO_DOCU_RUC}}">RUC</option>
                      <option value="{{App\Persona::TIPO_DOCU_OTROS}}">OTROS</option>
                    </select>
                </div>

                <div class="row">
                  <div class="col-md-2"></div>
                  <label class="label-texto col-md-2">Telefono:</label>
                  <input type="text"  id="telefono" name="telefono" class="col-md-6 form-control" placeholder="Ingrese Telefono">
                </div>

                <div class="row">
                  <div class="col-md-2"></div>
                  <label class="label-texto col-md-2">Email:</label>
                  <input type="text"  id="email" name="email" class="col-md-6 form-control" placeholder="Ingrese Edad" required>
                </div>
        </div>

        <!-- Modal footer -->
        <div class="modal-footer">
         <button type="submit" id="btnLimpiarPersona" class="btn btn-info">REGISTRAR</button>
          <button type="button" class="btn btn-danger" data-dismiss="modal">CERRAR</button>
        </div>
        </form>


          </div>
      </div>
  </div>
<!--Fin Modal-->

<!--Modal Nuevo Tipo Actividad-->
<div class="modal" id="modal-tipo-actividad">
    <div class="modal-dialog">
      <div class="modal-content">

        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Nuevo Tipo Actividad</h4><br>


        </div>
        <form id="formLimpiarPerson" method="POST" action="{{ route('tipo_actividades.store')}}">
            @csrf
        <!-- Modal body -->
        <div class="modal-body">
                
                <div class="row">
                  <div class="col-md-2"></div>
                  <label class="label-texto col-md-2">Descripcion:</label>
                  <input type="text"  id="descripcion" name="descripcion" class="col-md-6  form-control" placeholder="Ingrese Descripcion">
                  <div class="col-md-2"></div>
                </div>
        </div>

        <!-- Modal footer -->
        <div class="modal-footer">
         <button type="submit" id="btnLimpiarPersona" class="btn btn-info">REGISTRAR</button>
          <button type="button" class="btn btn-danger" data-dismiss="modal">CERRAR</button>
        </div>
        </form>


          </div>
      </div>
  </div>
<!--Fin Modal-->

<!--Modal Nueva Actividad-->
<div class="modal" id="modal-actividad">
    <div class="modal-dialog">
      <div class="modal-content">

        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Nueva Actividad</h4><br>


        </div>
        <form id="formLimpiarPerson" method="POST" action="{{ route('actividades.store')}}">
            @csrf
        <!-- Modal body -->
        <div class="modal-body">
                
                <div class="row">
                  <div class="col-md-2"></div>
                  <label class="label-texto col-md-2">Duracion:</label>
                  <input type="time"  id="duracion" name="duracion" class="col-md-6  form-control" placeholder="Ingrese Descripcion">
                  <div class="col-md-2"></div>
                </div>

                <div class="row">
                  <div class="col-md-2"></div>
                  <label class="label-texto col-md-2">Tipo Actividad:</label>
                  <select class="form-control col-md-6 process" id="idt_actividad" name="idt_actividad">
                      @foreach (App\TipoActividad::all() as $item)
                      <option value="{{$item->idt_actividad}}">{{$item->descripcion}}</option>
                      @endforeach
                      
                    </select>
                  <div class="col-md-2"></div>
                </div>

                <div class="row">
                    <div class="col-md-2"></div>
                    <label class="label-texto col-md-2">Persona:</label>
                    <select class="form-control col-md-6 process" id="id_persona" name="id_persona">
                        @foreach (App\Persona::all() as $item)
                        <option value="{{$item->id_persona}}">{{$item->nombre}}</option>
                        @endforeach
                        
                      </select>
                    <div class="col-md-2"></div>
                  </div>
        </div>

        <!-- Modal footer -->
        <div class="modal-footer">
         <button type="submit" id="btnLimpiarPersona" class="btn btn-info">REGISTRAR</button>
          <button type="button" class="btn btn-danger" data-dismiss="modal">CERRAR</button>
        </div>
        </form>


          </div>
      </div>
  </div>
<!--Fin Modal-->


  <!--Vista Orden Asignada-->
  <!--MODAL Activo-->
 <div class="modal" id="modal-vista-orden">
    <div class="modal-dialog">
      <div class="modal-content">

        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title"></h4><br>


        </div>
        <form id="formLimpiar-2" >
        <!-- Modal body -->
        <div class="modal-body">

                <div class="row">
                  <div class="col-md-1"></div>
                  <label class="label-texto col-md-4">Asignacion</label>
                  <select class="form-control col-md-6" id="tipo_proceso" name="tipo_proceso">
                  </select>
                </div>

                <div class="row">
                  <div class="col-md-1"></div>
                  <label class="label-texto col-md-4">Envio de Escrito:</label>
                  <select class="form-control col-md-6" id="tipo_proceso" name="tipo_proceso">
                  </select>
                </div>
                <div class="row">
                  <div class="col-md-1"></div>
                  <label class="label-texto col-md-4">Nº Expediente:</label>
                  <input type="text"  id="nombre" name="nombre" class="col-md-6  form-control" required>
                </div>

                <div class="row">
                  <div class="col-md-1"></div>
                  <label class="label-texto col-md-4">Respuesta Juzgado:</label>
                  <select class="form-control col-md-6" id="tipo_proceso" name="tipo_proceso">
                  </select>
                </div>

                <div class="row">
                  <div class="col-md-1"></div>
                  <label class="label-texto col-md-4">Juez:</label>
                  <input type="text"  id="apellido" name="apellido" class="col-md-6 form-control" required>
                </div>

        </div>

        <!-- Modal footer -->
        <div class="modal-footer">
         <button type="button" id="btnLimpiar" class="btn btn-info"  onclick='guardar_orden();'>REGISTRAR</button>
          <button type="button" class="btn btn-danger" data-dismiss="modal">CERRAR</button>
        </div>
        </form>


          </div>
      </div>
  </div>
  <!---->

<!--Vista Nueva Actividad-->
  <div class="modal" id="modal-nueva-actividad">
    <div class="modal-dialog">
      <div class="modal-content">

        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Nueva Actividad</h4><br>


        </div>
        <form id="formLimpiar-2" >
        <!-- Modal body -->
        <div class="modal-body">
                <div class="row">
                  <div class="col-md-1"></div>
                  <label class="label-texto col-md-4">Caso:</label>
                  <input type="text"  id="nombre" name="nombre" class="col-md-6  form-control" required>
                </div>

                <div class="row">
                  <div class="col-md-1"></div>
                  <label class="label-texto col-md-4">Nº Expediente:</label>
                  <input type="text"  id="nombre" name="nombre" class="col-md-6  form-control" required>
                </div>

                <div class="row">
                  <div class="col-md-1"></div>
                  <label class="label-texto col-md-4">Tipo Actividad</label>
                  <select class="form-control col-md-6" id="tipo_proceso" name="tipo_proceso">
                  </select>
                </div>

                <div class="row">
                  <div class="col-md-1"></div>
                  <label class="label-texto col-md-4">Juzgado:</label>
                  <input type="text"  id="nombre" name="nombre" class="col-md-6  form-control" required>
                </div>

                <div class="row">
                  <div class="col-md-1"></div>
                  <label class="label-texto col-md-4">Observacion 1:</label>
                  <input type="text"  id="nombre" name="nombre" class="col-md-6  form-control" required>
                </div>

                <div class="row">
                  <div class="col-md-1"></div>
                  <label class="label-texto col-md-4">Observacion 2:</label>
                  <textarea class="form-control col-md-6 " id="mensaje" name="mensaje" rows="5" required="required"></textarea>
                </div>


        </div>

        <!-- Modal footer -->
        <div class="modal-footer">
         <button type="button" id="btnLimpiar" class="btn btn-info"  onclick='registrar_actividad();'>REGISTRAR</button>
          <button type="button" class="btn btn-danger" data-dismiss="modal">CERRAR</button>
        </div>
        </form>


          </div>
      </div>
  </div>
  <!---->

  <!--
  <script src="//code.jquery.com/jquery-1.11.0.min.js"></script>


	<script>


	   $("#btnLimpiar").click(function(event) {
	     $("#formLimpiar")[0].reset();
	   });
	</script>
  -->
