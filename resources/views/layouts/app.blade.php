<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">

    <!-- Styles -->
   <!-- <link href="{{ asset('css/app.css') }}" rel="stylesheet"> -->
    <link rel="stylesheet" href="{{ asset('lib/alertify/css/alertify.min.css')}}" />
    <link rel="stylesheet" href="{{ asset('lib/alertify/css/themes/default.min.css')}}" />
    <link rel="stylesheet" href="{{ asset('lib/lineAwesome/css/line-awesome.min.css')}}" />
    <!--end::Fonts -->
    <!--begin::Page Vendors Styles(used by this page) -->
    <!--end::Page Vendors Styles -->
    <!--begin::Global Theme Styles(used by all pages) -->
    <link href="{{asset('assets/plugins/global/plugins.bundle.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/css/style.bundle.css')}}" rel="stylesheet" type="text/css" />

    <!--end::Global Theme Styles -->
    <!--begin::Layout Skins(used by all pages) -->
    <link href="{{asset('assets/css/skins/header/base/light.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/css/skins/header/menu/light.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/css/skins/brand/navy.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/css/skins/aside/navy.css')}}" rel="stylesheet" type="text/css" />

    <link href="{{ asset('css/site.css') }}" rel="stylesheet">
</head>
<body class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--enabled kt-subheader--transparent kt-aside--enabled kt-aside--fixed kt-page--loading bg-dark">
    <div id="app">
        @guest
         @else
         <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
              @include('layouts.menu.ventanaModal')
              @include('layouts.menu.header_movil')
                <div class="kt-grid kt-grid--hor kt-grid--root">
                <!-- begin:: Page -->
                <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">

                     @include('layouts.menu.navAside')

                    <!-- begin:: Wrapper -->
                    <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">

                       @include('layouts.menu.topHeader')

                        <!-- begin:: Content -->
                       
						<main class="py-4">
                                @yield('content')
                            </main>
                            <!-- end:: Content -->

                    </div>

                    <!-- end:: Wrapper -->
                </div>

                <!-- end:: Page -->
            </div>
        </nav>
         @endguest
		 
		  

          
    </div>
<script>
        var KTAppOptions = {
            "colors": {
                "state": {
                    "brand": "#5d78ff",
                    "metal": "#c4c5d6",
                    "light": "#ffffff",
                    "accent": "#00c5dc",
                    "primary": "#5867dd",
                    "success": "#34bfa3",
                    "info": "#36a3f7",
                    "warning": "#ffb822",
                    "danger": "#fd3995",
                    "focus": "#9816f4"
                },
                "base": {
                    "label": [
                        "#c5cbe3",
                        "#a1a8c3",
                        "#3d4465",
                        "#3e4466"
                    ],
                    "shape": [
                        "#f0f3ff",
                        "#d9dffa",
                        "#afb4d4",
                        "#646c9a"
                    ]
                }
            }
        };
    </script>

    <!-- end::Global Config -->
    <!--begin::Global Theme Bundle(used by all pages) -->
    <script src="{{asset('assets/plugins/global/plugins.bundle.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/js/scripts.bundle.js')}}" type="text/javascript"></script>
    <!--end::Global Theme Bundle -->
    <!--begin::Page Vendors(used by this page) -->
    <script src="{{asset('assets/plugins/custom/fullcalendar/fullcalendar.bundle.js')}}" type="text/javascript"></script>
    <!--end::Page Vendors -->
    <!--begin::Page Scripts(used by this page) -->
    <script src="{{asset('assets/js/pages/dashboard.js')}}" type="text/javascript"></script>
    <script src="{{asset('lib/alertify/alertify.min.js')}}"></script>

    <script src="{{asset('js/site.js')}}" asp-append-version="true"></script>

</body>
</html>
