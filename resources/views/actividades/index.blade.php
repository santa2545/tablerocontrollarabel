@extends('layouts.app')
@section('content')

    <div class="container-fluid">
        <div id="containerPrimary">
            <div class="row">
                <div class="col-md-4">

                </div>

                <div class="col-md-4">
                    <div class="card">
                        <div class="card-header" style="text-align: center;">
                        Nueva Actividad
                        </div>
                        <div class="card-body" style="text-align: center;">
                        <button type="button" class="btn btn-info" onclick="NuevaActividad();">Nuevo (+)</button>
                        </div>
                    </div>

                </div>

                <div class="col-md-4">

                </div>

            </div>
        </div>

        <div id="containerSecondary " class="bg-white">
            <table class="table table-hover table-bordered table-sm">
                <thead>
                    <tr>
                        <th>Duracion</th>
                        <th>Tipo Actividad</th>
                        <th>Persona</th>
                       
                    </tr>
                </thead>
                <tbody>
                        @foreach ($actividades as $item)
                    <tr>
                       
                         
                         <td>{{$item->duracion}}</td>
                         <td>{{$item->TipoActividad->descripcion}}</td>
                         <td>{{$item->persona->nombre}}</td>
                      
                    </tr>
                    @endforeach
                </tbody>
            </table>
            {!! $actividades->links()!!}
        </div>

    </div>

@stop