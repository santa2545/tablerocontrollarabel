@extends('layouts.app')
@section('content')

    <div class="container-fluid">
        <div id="containerPrimary">
            <div class="row">
                <div class="col-md-4">

                </div>

                <div class="col-md-4">
                    <div class="card">
                        <div class="card-header" style="text-align: center;">
                        Nueva Persona
                        </div>
                        <div class="card-body" style="text-align: center;">
                        <button type="button" class="btn btn-info" onclick="Nueva_persona();">Nuevo (+)</button>
                        </div>
                    </div>

                </div>

                <div class="col-md-4">

                </div>

            </div>
        </div>

        <div id="containerSecondary " class="bg-white">
            <table class="table table-hover table-bordered table-sm">
                <thead>
                    <tr>
                        <th>Codigo</th>
                        <th>Nombre</th>
                        <th>Documento</th>
                        <th>Tipo Documento</th>
                        <th>Telefono</th>
                        <th>Email</th>
                    </tr>
                </thead>
                <tbody>
                        @foreach ($personas as $item)
                    <tr>
                       
                         <td>{{$item->id_persona}}</td>
                         <td>{{$item->nombre}}</td>
                         <td>{{$item->documento}}</td>
                         <td>{{$item->tipo_docu}}</td>
                         <td>{{$item->telefono}}</td>
                         <td>{{$item->email}}</td>
                      
                    </tr>
                    @endforeach
                </tbody>
            </table>
            {!! $personas->links()!!}
        </div>

    </div>

@stop