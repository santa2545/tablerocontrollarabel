@extends('layouts.app')

@section('content')
<!--<div class="container"> -->
<div class="card card-login mx-auto mt-5" style="max-width: 25rem;">
        <!--<div class="card-header" style="text-align: center;">Ingreso</div>-->

        <div class="card-body">

        <!-- Default form login -->
            <form method="POST" action="{{ route('login') }}" name="login" method="POST" id="form" class="text-center border border-light p-5">
                  @csrf
                <p class="h4 mb-4">Ingresar</p>

                <!-- Email -->
                  <input id="email" type="email" class="form-control mb-4 @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror

                <!-- Password -->
                <input id="password" type="password" class="form-control mb-4 @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror

                <div class="enter" id="logearse">
                    <!-- Sign in button -->
                    <input type="submit" id="logearse" name="submit" class="btn btn-info btn-block my-4" value="Ingresar" type="submit" />
                </div>

           </form>
        </div>
      </div>
<!--</div>-->
@endsection
