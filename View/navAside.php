<?php  ?>

<!-- begin:: Aside -->
            <button class="kt-aside-close " id="kt_aside_close_btn"><i class="la la-close"></i></button>
            <div class="kt-aside  kt-aside--fixed bg-color-sidebar" id="kt_aside">

                <div class="kt-aside__brand kt-grid__item bg-color-sidebar" id="kt_aside_brand">
                    <div class="kt-aside__brand-logo">
                        <a href="#">
                            <!--<img alt="Logo" src="img/logo-setra.jpg" style="width:88px; height:50px;"/>-->
                        </a>
                    </div>
                    <div class="kt-aside__brand-tools">
                        <button class="kt-aside__brand-aside-toggler kt-aside__brand-aside-toggler--left" id="kt_aside_toggler"><span></span></button>
                    </div>
                </div>

                <div class="kt-aside-menu-wrapper kt-grid__item kt-grid__item--fluid" id="kt_aside_menu_wrapper">
                    <div id="kt_aside_menu" class="kt-aside-menu bg-color-sidebar" data-ktmenu-vertical="1" data-ktmenu-scroll="1" data-ktmenu-dropdown-timeout="500">
                        <ul class="kt-menu__nav ">
                            <li class="kt-menu__item  kt-menu__item--submenu kt-menu__item--open kt-menu__item--here" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
                                <a href="Dashboard.php" class="kt-menu__link bg-color-sidebar"><i class="kt-menu__link-icon flaticon2-graphic"></i><span class="kt-menu__link-text">Dashboards</span></a>
                            </li>
                            <li class="kt-menu__section ">
                                <h4 class="kt-menu__section-text colorTituloGrupal">Recepcion</h4>
                                <i class="kt-menu__section-icon flaticon-more-v2"></i>
                            </li>

                            <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
                                <a href="Persona.php" class="kt-menu__link"><i class="kt-menu__link-icon flaticon2-copy"></i><span class="kt-menu__link-text">Persona</span></a>
                                <div class="kt-menu__submenu ">
                                    <span class="kt-menu__arrow"></span>
                                </div>
                            </li>

                            <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
                                <a href="NewProcess.php" class="kt-menu__link"><i class="kt-menu__link-icon flaticon2-copy"></i><span class="kt-menu__link-text">Proceso Judicial</span></a>
                                <div class="kt-menu__submenu ">
                                    <span class="kt-menu__arrow"></span>
                                </div>
                            </li>
                            <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
                                <a href="Disponibilidad.php" class="kt-menu__link"><i class="kt-menu__link-icon flaticon2-checking"></i><span class="kt-menu__link-text">Disponibilidad</span></a>
                                <div class="kt-menu__submenu ">
                                    <span class="kt-menu__arrow"></span>
                                </div>
                            </li>
                            <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
                                <a class="kt-menu__link"><i class="kt-menu__link-icon flaticon2-copy"></i><span class="kt-menu__link-text">Tarifario</span></a>
                                <div class="kt-menu__submenu ">
                                    <span class="kt-menu__arrow"></span>
                                </div>
                            </li>
                            <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
                                <a class="kt-menu__link"><i class="kt-menu__link-icon flaticon2-percentage "></i><span class="kt-menu__link-text">Honorario de Abogados</span></a>
                                <div class="kt-menu__submenu ">
                                    <span class="kt-menu__arrow"></span>
                                </div>
                            </li>

                            <li class="kt-menu__section ">
                                <h4 class="kt-menu__section-text colorTituloGrupal">Abogados</h4>
                                <i class="kt-menu__section-icon flaticon-more-v2"></i>
                            </li>

                            <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
                                <a href="Cronograma.php" class="kt-menu__link "><i class="kt-menu__link-icon flaticon2-calendar-2"></i><span class="kt-menu__link-text">Cronograma</span></a>
                                <div class="kt-menu__submenu ">
                                    <span class="kt-menu__arrow"></span>
                                </div>
                            </li>
                            <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
                                <a href="Ordenes.php" class="kt-menu__link"><i class="kt-menu__link-icon flaticon2-copy"></i><span class="kt-menu__link-text">Ordenes Asignadas</span></a>
                                <div class="kt-menu__submenu ">
                                    <span class="kt-menu__arrow"></span>
                                </div>
                            </li>
                            <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
                                <a class="kt-menu__link "><i class="kt-menu__link-icon flaticon2-gear"></i><span class="kt-menu__link-text">Mantenimiento de Caso</span></a>
                                <div class="kt-menu__submenu ">
                                    <span class="kt-menu__arrow"></span>
                                </div>
                            </li>
                            <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
                                <a class="kt-menu__link"><i class="kt-menu__link-icon flaticon2-contract"></i><span class="kt-menu__link-text">Casos Asignados</span></a>
                                <div class="kt-menu__submenu ">
                                    <span class="kt-menu__arrow"></span>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>

                <!-- begin:: Aside Footer -->
                <div class="kt-aside__footer kt-grid__item" id="kt_aside_footer">
                    <div class="kt-aside__footer-nav bg-color-sidebar">
                        <div class="kt-aside__footer-item">
                            <a href="#" class="btn btn-icon"><i class="flaticon2-gear"></i></a>
                        </div>
                        <div class="kt-aside__footer-item">
                            <a href="#" class="btn btn-icon"><i class="flaticon2-cube"></i></a>
                        </div>
                        <div class="kt-aside__footer-item">
                            <a href="#" class="btn btn-icon"><i class="flaticon2-bell-alarm-symbol"></i></a>
                        </div>
                        <div class="kt-aside__footer-item">
                            <button type="button" class="btn btn-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="flaticon2-add"></i>
                            </button>
                            <div class="dropdown-menu dropdown-menu-left">
                                <ul class="kt-nav">
                                    <li class="kt-nav__section kt-nav__section--first">
                                        <span class="kt-nav__section-text">Export Tools</span>
                                    </li>
                                    <li class="kt-nav__item">
                                        <a href="#" class="kt-nav__link">
                                            <i class="kt-nav__link-icon la la-print"></i>
                                            <span class="kt-nav__link-text">Print</span>
                                        </a>
                                    </li>
                                    <li class="kt-nav__item">
                                        <a href="#" class="kt-nav__link">
                                            <i class="kt-nav__link-icon la la-copy"></i>
                                            <span class="kt-nav__link-text">Copy</span>
                                        </a>
                                    </li>
                                    <li class="kt-nav__item">
                                        <a href="#" class="kt-nav__link">
                                            <i class="kt-nav__link-icon la la-file-excel-o"></i>
                                            <span class="kt-nav__link-text">Excel</span>
                                        </a>
                                    </li>
                                    <li class="kt-nav__item">
                                        <a href="#" class="kt-nav__link">
                                            <i class="kt-nav__link-icon la la-file-text-o"></i>
                                            <span class="kt-nav__link-text">CSV</span>
                                        </a>
                                    </li>
                                    <li class="kt-nav__item">
                                        <a href="#" class="kt-nav__link">
                                            <i class="kt-nav__link-icon la la-file-pdf-o"></i>
                                            <span class="kt-nav__link-text">PDF</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="kt-aside__footer-item">
                            <a href="#" class="btn btn-icon"><i class="flaticon2-calendar-2"></i></a>
                        </div>
                    </div>
                </div>
                <!-- end:: Aside Footer-->
            </div>
<!-- end:: Aside -->