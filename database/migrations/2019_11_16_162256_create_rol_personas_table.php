<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRolPersonasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rol_personas', function (Blueprint $table) {
            $table->bigIncrements('idRol');
            $table->string('rol');
            $table->unsignedBigInteger('idt_persona');
              $table->foreign('idt_persona')->references('idt_persona')->on('tipo_personas')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rol_personas');
    }
}
