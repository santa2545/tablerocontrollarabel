<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActividadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('actividads', function (Blueprint $table) {
            $table->bigIncrements('id_actividad');
            $table->string('duracion')->nullable();//tiempo de duracion de la actividad
            $table->string('descripcion',500)->nullable();
            $table->unsignedBigInteger('idt_actividad');
            $table->unsignedBigInteger('id_persona');
            $table->foreign('idt_actividad')->references('idt_actividad')->on('tipo_actividads')
                    ->onUpdate('cascade');
             $table->foreign('id_persona')->references('id_persona')->on('personas')
                    ->onUpdate('cascade');
            $table->timestamps();
        });
    }

/* int not null primary key AUTO_INCREMENT,
     timestamp,
     time,
     int,
     int,
    foreign key (idt_actividad) references tipoActividad(idt_actividad),
    foreign key (id_persona) references persona(id_persona)

*/
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('actividads');
    }
}
