<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExpedientesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('expedientes', function (Blueprint $table) {
            $table->bigIncrements('id_expediente');
            $table->integer('nro_exp');
            $table->string('descripcion', 500);
            $table->unsignedBigInteger('id_persona');
            $table->unsignedBigInteger('id_proceso');
            $table->unsignedBigInteger('id_estado');
            $table->unsignedBigInteger('id_juzgado');
            $table->unsignedBigInteger('id_actividad');
            $table->foreign('id_persona')->references('id_persona')->on('personas')
                ->onUpdate('cascade');
            $table->foreign('id_proceso')->references('id_proceso')->on('procesos')
                ->onUpdate('cascade');
            $table->foreign('id_estado')->references('id_estado')->on('estados')
                ->onUpdate('cascade');
            $table->foreign('id_juzgado')->references('id_juzgado')->on('juzgados')
                ->onUpdate('cascade');
            $table->foreign('id_actividad')->references('id_actividad')->on('actividads')
                ->onUpdate('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('expedientes');
    }
}
