var HELPER_API = function (_type, _datatype, _path, _parameters = null) {

    var _response; 
    var _url = eval('ENDPOINT_' + ENVIROMENT) + _path;
    var _datatype = 'json';
    var _contenttype = 'application/json; charset=utf-8';

    if (_type == HTTP_GET || _type == HTTP_DELETE)
    {
        _response = $.ajax({
            type: _type,
            url: _url,
            dataType: _datatype,
            contentType: _contenttype,
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR);
                console.log(errorThrown);
                console.log(textStatus);
            }
        });
    }
    else
    {
        _response = $.ajax({
            type: _type,
            url: _url,
            dataType: _datatype,
            contentType: _contenttype,
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR);
                console.log(errorThrown);
                console.log(textStatus);
            }
        });
    }

    return _response;
}

