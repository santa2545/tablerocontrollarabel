var HELPER_POPUP = function (_type, _url, _parameters = null) {

    var _datatype = 'HTML';

    $('#' + CONTAINER_SECONDARY).removeClass("d-none");
    $('#' + CONTAINER_PRIMARY).addClass("d-none");

    $.ajax({
        type: _type,
        url: _url,
        dataType: _datatype,
        success: function (data) {
            $('#' + CONTAINER_SECONDARY).html(data);
        }
    });
}

var HELPER_POPUP_CLOSE = function () {

    $('#' + CONTAINER_SECONDARY).html("");
    $('#' + CONTAINER_SECONDARY).addClass("d-none");
    $('#' + CONTAINER_PRIMARY).removeClass("d-none");
}

