<?php

namespace App\Http\Controllers;

use App\TipoProceso;
use Illuminate\Http\Request;

class TipoProcesoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TipoProceso  $tipoProceso
     * @return \Illuminate\Http\Response
     */
    public function show(TipoProceso $tipoProceso)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TipoProceso  $tipoProceso
     * @return \Illuminate\Http\Response
     */
    public function edit(TipoProceso $tipoProceso)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TipoProceso  $tipoProceso
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TipoProceso $tipoProceso)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TipoProceso  $tipoProceso
     * @return \Illuminate\Http\Response
     */
    public function destroy(TipoProceso $tipoProceso)
    {
        //
    }
}
