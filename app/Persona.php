<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Persona extends Model
{
     protected $primaryKey = 'id_persona';

     const GENERO_M = 'M';
     const GENERO_F = 'F';
     const GENERO_OTROS = 'OTROS';

     const TIPO_DOCU_DNI = 'DNI';
     const TIPO_DOCU_RUC = 'RUC';
     const TIPO_DOCU_OTROS = 'OTROS';

     protected $fillable = ['idrol','nombre','documento','genero','tipo_docu','telefono','email'];


}
