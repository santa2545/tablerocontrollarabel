<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Actividad extends Model
{
     protected $primaryKey = 'id_actividad';


     protected $fillable = ['duracion','descripcion','idt_actividad','id_persona'];



     public function persona(){

          return $this->belongsTo('App\Persona', 'id_persona', 'id_persona');
     }

     public function TipoActividad(){

          return $this->belongsTo('App\TipoActividad', 'idt_actividad', 'idt_actividad');
     }

}
